#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"
//测试小堆插入删除
void testHeap()
{
	Heap* php = (Heap*)malloc(sizeof(Heap));
	HeapInit(php);
	HeapPush(php, 10);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 20);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 30);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 15);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 5);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 40);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPush(php, 15);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPop(php);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPop(php);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPop(php);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPop(php);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
	HeapPop(php);
	printf("Size:%d\n", HeapSize(php));
	printf("Top:%d\n", HeapTop(php));
}

//int main()
//{
//	////测试堆
//	//test1();
//	return 0;
//}

////该方法可以，但是由缺点
////弊端1.现有一个堆 2.空间消耗
//void HeapSort(int* arr, int size)
//{
//	Heap hp;
//	HeapInit(&hp);
//	//建堆O(NlogN)
//	for (int i = 0; i < size; i++)
//	{
//		HeapPush(&hp, arr[i]);
//	}
//	//N*logN
//	for (int i = 0; i < size; i++)
//	{
//		arr[i] = HeapTop(&hp);
//		HeapPop(&hp);
//	}
//	for (int i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}

///复杂度O(NlogN)
void HeapSort(int* arr, int size)
{
	//升序 -- 建大堆
	//降序 -- 建小堆
	
	//向上调整建堆---o(nlogn)
	for (int i = 1; i < size; i++)
	{
		AdjustUp(arr, i);
	}
	
	////向下调整建堆(从最后一个叶子节点的父亲开始调整)---O(n)
	//for (int i = (size-1-1)/2; i >= 0; i--)
	//{
	//	AdjustDown(arr, size, i);
	//}

	int end = size - 1;
	//O(NlogN)
	while (end > 0)
	{
		//最小的元素和最后一个元素交换
		Swap(&arr[end], &arr[0]);
		//向下调整 -- 找出次小的元素O(logn)
		AdjustDown(arr, end, 0);
		//调整时减去已经有序的元素
		end--;
	}

}
void testHeapSort()
{
	srand((unsigned int)(time(0)));
	int arr[20] = { 0 };
	for (int i = 0; i < 20; i++)
	{
		arr[i] = rand() % 100;
	}
	HeapSort(arr, sizeof(arr) / sizeof(arr[0]));
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]);i++)
	{
		printf("%d ", arr[i]);
	}
}

//优质筛选问题
void TestTopK(int* arr, int n, int k)
{
	int* kminHeap = (int*)malloc(sizeof(int) * k);
	if (kminHeap == NULL)
	{
		perror("kminHeap申请失败:");
		exit(EXIT_FAILURE);
	}

	////思路1排序----当数据很大时无法实现，内存中没有那么多空间可以开辟
	//  时间复杂度O(nlogn)
	//HeapSort(arr, size);
	//for (int i = 0; i < k; i++)
	//{
	//	kminHeap[i] = arr[i];
	//}
	//for (int i = 0; i < k; i++)
	//{
	//	printf("%d ", kminHeap[i]);
	//}

	//思路2（重点）
	//找前k个最小的 -- 建大堆
	//找前k个最大的 -- 建小堆

	//1.先将k个数据放入堆中，在将n-k个数据依次和堆顶比较：

	//2.如果寻找前个k最小的，将比大堆堆顶小的元素放入堆中，进行向下调整
	//  如果寻找前个k最大的，将比小堆堆顶大的元素放入堆中，进行向下调整

	//前k个元素建立堆
	for (int i = 0; i < k; i++)
	{
		kminHeap[i] = arr[i];
	}

	//F(n)=(n - k) * (H - 1) = (n - k) * (log(k+1) - 1) -- O(nlogk)
	for (int i = k; i < n; i++)
	{
		if (arr[i] > kminHeap[0])
		{
			kminHeap[0] = arr[i];
			AdjustDown(kminHeap, k, 0);//AdjustDown建小堆
		}
	}
	for (int i = 0; i < k; i++)
	{
		printf("%d ", kminHeap[i]);
	}
	free(kminHeap);
}

int* TopK(int* arr, int n, int k)//O(nlogn)
{
	int* kminHeap = (int*)malloc(sizeof(int) * k);
	if (kminHeap == NULL)
	{
		perror("kminHeap申请失败:");
		exit(EXIT_FAILURE);
	}

	for (int i = 0; i < k; i++)
	{
		kminHeap[i] = arr[i];
	}
	//F(n)=(n - k) * (H - 1) = (n - k) * (log(k+1) - 1) -- O(nlogk)
	for (int i = k; i < n; i++)
	{
		if (arr[i] > kminHeap[0])
		{
			kminHeap[0] = arr[i];
			AdjustDown(kminHeap, k, 0);//AdjustDown建小堆
		}
	}
	return kminHeap;
}

//测试TopK
void testTopK()
{
	srand((unsigned long)time(0));
	int arr[20] = { 0 };
	for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		arr[i] = rand() % 100;
	}
	arr[10] += 100;
	arr[15] += 100;
	arr[5] += 100;
	arr[12] += 100;
	int k = 4;
	int* topK = TopK(arr, sizeof(arr) / sizeof(arr[0]), k);
	for (int i = 0; i < k; i++)
	{
		printf("%d ", topK[i]);
	}
}
int main()
{
	testHeapSort();
	//testTopK();
	//testHeap();
}