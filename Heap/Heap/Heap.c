#define _CRT_SECURE_NO_WARNINGS 1
#include "Heap.h"
//堆的初始化
void HeapInit(Heap* php)
{
	assert(php);
	php->a = NULL;
	php->capacity = php->size = 0;
}
//堆的销毁
void HeapDestroy(Heap* php)
{
	assert(php);
	free(php->a);
	php->a = NULL;
	php->capacity = php->size = 0;
}

void Swap(HPDataType* a, HPDataType* b)
{
	HPDataType tmp = *a;
	*a = *b;
	*b = tmp;
}
//向上调整
void AdjustUp(int* a, int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		//小堆：孩子比父亲大，如果孩子小于父亲，需交换
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (parent - 1) / 2;
		}
		
		////大堆：孩子比父亲小，如果孩子大于父亲，需交换
		//if (a[child] > a[parent])
		//{
		//	Swap(&a[child], &a[parent]);
		//	child = parent;
		//	parent = (parent - 1) / 2;
		//}
		else
		{
			break;
		}
	}
}
//堆的插入--时间复杂度为O(logN)
void HeapPush(Heap* php, HPDataType x)
{
	assert(php);
	//判断是否需要扩容
	if (php->capacity == php->size)
	{
		php->capacity = php->capacity == 0 ? 4 : 2 * php->capacity;
		HPDataType* tmp = realloc(php->a, sizeof(HPDataType) * php->capacity);
		if (NULL != tmp)
			php->a = tmp;
	}
	//插入元素
	php->a[php->size] = x;
	php->size++;
	//向上调整
	AdjustUp(php->a, php->size - 1);
}

//向下调整
void AdjustDown(int* a, int size, int parent)
{
	int child = parent * 2 + 1;
	//保证左孩子下标在有效范围内
	while (child < size)
	{
		//小堆逻辑
		//让child是左右孩子中较小的那一个
		if (child + 1 < size && a[child] > a[child + 1])//左孩子存在右孩子不一定存在
		{
			child++;
		}
		//小堆孩子大于父亲，如果孩子比父亲小需要交换
		if (a[child] < a[parent])
		{
			Swap(&a[parent], &a[child]);
		}
		
		////大堆逻辑
		////child是左右孩子中较小的那一个
		//if (child + 1 < size && a[child] < a[child + 1])//左孩子存在右孩子不一定存在
		//{
		//	child++;
		//}
		////大堆孩子小于父亲，如果孩子比父亲大需要交换
		//if (a[child] > a[parent])
		//{
		//	Swap(&a[parent], &a[child]);
		//}

		else
		{
			break;
		}
		parent = child;
		child = parent * 2 + 1;
	}
}
//堆的删除(删除堆顶)--时间复杂度为O(logN)
void HeapPop(Heap* php)
{
	assert(php->size > 0);
	//交换顶和最后一个节点
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;
		//向下调整
	AdjustDown(php->a, php->size, 0);
}

//堆的判空
bool HeapEmpty(Heap* php)
{
	assert(php);
	return php->size == 0;
}
//取出堆顶元素
HPDataType HeapTop(Heap* php)
{
	assert(php); 
	assert(!HeapEmpty(php));
	return php->a[0];
}
//堆的数据个数
int HeapSize(Heap* php)
{
	assert(php);
	assert(!HeapEmpty(php));
	return php->size;
}