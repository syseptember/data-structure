#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
#include <time.h>
typedef int HPDataType;
//堆的逻辑结构是完全二叉树
//由于堆属于完全二叉树，因此可以使用数组存放堆的节点
//堆的结构和顺序表一样
typedef struct Heap
{
	HPDataType* a;
	int size;
	int capacity;
}Heap;
//交换
extern void Swap(HPDataType* a, HPDataType* b);
//堆的初始化
extern void HeapInit(Heap* php);
//堆的销毁
extern void HeapDestroy(Heap* php);
//堆的判空
extern bool HeapEmpty(Heap* php);
//向上调整
extern void AdjustUp(HPDataType* a, int child);
//向下调整
extern void AdjustDown(HPDataType* a, int size, int parent);
//堆的插入
extern void HeapPush(Heap* php, HPDataType x);
//堆的删除(删除堆顶元素)
extern void HeapPop(Heap* php);
//取出堆顶元素
extern HPDataType HeapTop(Heap* php);
//堆的数据个数
extern int HeapSize(Heap* php);
