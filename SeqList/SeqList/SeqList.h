#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

typedef int SeqListDataType;

typedef struct
{
	SeqListDataType* a;
	int size;
	int capcity;
}SeqList;

void SeqListInit(SeqList* ps);
void SeqListCheckCapcity(SeqList* ps);
//��ӡ
void SeqListPrint(SeqList* ps);
//β��
void SeqListPushBack(SeqList* ps, SeqListDataType pos);
//βɾ
void SeqListPopBack(SeqList* ps);
//ͷ��
void SeqListPushFront(SeqList* ps, SeqListDataType pos);
//ͷɾ
void SeqListPopFront(SeqList* ps);
//ѡ�����
void SeqListInsert(SeqList* ps, int pos, SeqListDataType data);
//ѡ��ɾ��
void SeqListErase(SeqList* ps, int pos);