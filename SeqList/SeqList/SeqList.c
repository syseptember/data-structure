#define _CRT_SECURE_NO_WARNINGS 1
#include "SeqList.h"

void SeqListInit(SeqList* ps)
{
	ps->a = NULL;
	ps->capcity = ps->size = 0;
}

void SeqListPrint(SeqList* ps)
{
	for (int i = 0; i < ps->size; i++)
	{
		printf("%d ", ps->a[i]);
	}
	printf("\n");
}

void SeqListCheckCapcity(SeqList* ps)
{
	if (ps->size == ps->capcity)
	{
		ps->capcity = ps->capcity == 0 ? 4 : 2 * ps->capcity;
		SeqListDataType* tmp = (SeqListDataType*)
			realloc(ps->a, sizeof(SeqList) * (2 * ps->capcity));
		if (tmp != NULL) ps->a = tmp;
	}
}

void SeqListPushBack(SeqList* ps, int data)
{

	////当顺序表的容量满了
	//SeqListCheckCapcity(ps);
	//ps->a[ps->size] = data;
	//ps->size++;
	SeqListInsert(ps, ps->size, data);
}

void SeqListPopBack(SeqList* ps)
{
	/*assert(ps->size);
	ps->size--;*/
	SeqListErase(ps, ps->size - 1);
}

void SeqListPushFront(SeqList* ps, int data)
{
	//SeqListCheckCapcity(ps);
	//int cnt = 0;
	//while (cnt < ps->size)
	//{
	//	ps->a[ps->size - cnt] = ps->a[ps->size - 1 - cnt];
	//	cnt++;
	//}
	//ps->a[0] = data;
	//ps->size++;
	SeqListInsert(ps, 0, data);
}

void SeqListPopFront(SeqList* ps)
{
	//int begin = 1;
	//while (begin <= ps->size - 1)
	//{
	//	ps->a[begin - 1] = ps->a[begin];
	//	begin++;
	//}
	//ps->size--;
	SeqListErase(ps, 0);
}

void SeqListInsert(SeqList* ps, int pos, SeqListDataType data)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	SeqListCheckCapcity(ps);
	int end = ps->size;
	while (end >= pos + 1)
	{
		ps->a[end] = ps->a[end - 1];
		end--;
	}
	ps->a[pos] = data;
	ps->size++;
}

void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	assert(pos >= 0 && pos <= ps->size);
	int start = pos;
	while (start < ps->size - 1)
	{
		ps->a[start] = ps->a[start + 1];
		start++;
	}
	ps->size--;
}
