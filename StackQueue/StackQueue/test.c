#define _CRT_SECURE_NO_WARNINGS 1
#include "Stack.h"
#include "Queue.h"
void testStack()
{
	printf("这是一个栈的测试:");
	Stack ps;
	StackInit(&ps);
	StackPush(&ps, 1);
	StackPush(&ps, 2);
	StackPop(&ps);
	StackPush(&ps, 3);
	StackPush(&ps, 4);
	StackPop(&ps);
	StackPush(&ps, 5);
	while (!StackEmpty(&ps))
	{
		printf("Top:%d->", StackTop(&ps));
		StackPop(&ps);
	}
	printf("\nSize:%d\n", StackSize(&ps));
	StackDestroy(&ps);
}

void testQueue()
{
	printf("这是一个队列的测试:\n");
	Queue pq;
	assert(&pq);
	QueueInit(&pq);
	QueuePush(&pq, 1);
	QueuePush(&pq, 2);
	QueuePush(&pq, 3);
	QueuePush(&pq, 4);
	QueuePush(&pq, 5);
	QueuePush(&pq, 6);
	QueuePop(&pq);
	QueuePop(&pq);
	printf("Front:%d\n", QueueFront(&pq));
	printf("Back:%d\n", QueueBack(&pq));
	printf("Size:%d\n", QueueSize(&pq));
	while (!QueueEmpty(&pq))
	{
		printf("%d->", QueueFront(&pq));
		QueuePop(&pq);
	}
	QueueDestroy(&pq);
}
//int main()
//{
//	//testStack();
//	testQueue();
//	return 0;
//}
