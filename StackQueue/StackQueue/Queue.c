#define _CRT_SECURE_NO_WARNINGS 1
#include "Queue.h"
//队列的初始化
void QueueInit(Queue* pq)
{
	assert(pq);
	pq->head = pq->tail = NULL;
	pq->size = 0;
}

//队列是否为空
bool QueueEmpty(const Queue* pq)
{
	assert(pq);
	//return pq->head == NULL && pq->tail == NULL;
	return pq->size == 0;
}

//入队列
void QueuePush(Queue* pq, QDataType x)
{
	assert(pq);
	//创建新节点
	Qnode* newnode = (Qnode*)malloc(sizeof(Qnode));
	assert(newnode);
	newnode->val = x;
	newnode->next = NULL;
	//链表为空
	if (pq->tail == NULL)
	{
		assert(pq->head == NULL);//链表为空尾指针一定为空
		pq->head = pq->tail = newnode;
	}
	else
	{
		//直接尾插
		pq->tail->next = newnode;
		pq->tail = newnode;
	}
	pq->size++;
}

//出队列
void QueuePop(Queue* pq)
{
	assert(pq);
	//空队列不能删除
	assert(!QueueEmpty(pq));
	//只有1个节点
	if (pq->size == 1)
	{
		Qnode* tmp = pq->head;
		pq->head = pq->tail = NULL;
		free(tmp);
	}
	//2节点及以上
	else
	{
		//头删
		Qnode* tmp = pq->head;
		pq->head = pq->head->next;
		free(tmp);
	}
	pq->size--;
}

//获取队列头部元素
QDataType QueueFront(const Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->head->val;
}

//获取队列尾部元素
QDataType QueueBack(const Queue* pq)
{
	assert(pq);
	assert(!QueueEmpty(pq));
	return pq->tail->val;
}

//获取队列元素个数
int QueueSize(const Queue* pq)
{
	assert(pq);
	return pq->size;
}

//销毁队列
void QueueDestroy(Queue* pq)
{
	assert(pq);
	Qnode* cur = pq->head;
	while (cur)
	{
		Qnode* next = cur->next;
		free(cur);
		cur = next;
	}
	pq->head = pq->tail = 0;
	pq->size = 0;
}