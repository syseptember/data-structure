#define _CRT_SECURE_NO_WARNINGS 1
#include "Stack.h"

//栈的初始化
void StackInit(Stack* ps)
{
	assert(ps);
	ps->capacity = 0;
	ps->top = 0;//top初始化为0表示空栈
	ps->a = NULL;
}

//销毁栈
void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->a);
	ps->capacity = ps->top = 0;
}

//空栈
bool StackEmpty(const Stack* ps)
{
	return ps->top == 0;
}
//入栈
void StackPush(Stack* ps, STDataType x)
{
	assert(ps);
	if (ps->capacity == ps->top)//栈满扩容
	{
		ps->capacity = ps->capacity == 0 ? 4 : 2 * ps->capacity;
		//realloc可以解决扩容空栈
		STDataType* tmp = realloc(ps->a, sizeof(STDataType) * (ps->capacity));
		//成功开辟了空间在复制
		if (NULL == tmp)
		{
			free(ps->a);
			exit(0);
		}
		ps->a = tmp;
		assert(ps->a);
	}
	ps->a[ps->top] = x;
	ps->top++;
}
//出栈
void StackPop(Stack* ps)
{
	assert(ps);
	//空栈不能删除
	assert(!StackEmpty(ps));
	ps->top--;
}

//获取栈顶元素
STDataType StackTop(const Stack* ps)
{
	assert(ps);
	assert(!StackEmpty(ps));
	return ps->a[ps->top - 1];//top是指向栈顶下一个元素
}

//获取栈元素个数
int StackSize(const Stack* ps)
{
	assert(ps);
	return ps->top;
}