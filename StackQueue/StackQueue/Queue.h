#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int QDataType;
//队列由于具备先进后出的特点，使用链表实现效率更高，因为数组头插和头删的效率比链表低
typedef struct Qnode
{
	QDataType val;
	struct Qnode* next;
}Qnode;
typedef struct
{
	Qnode* head;//指向队列头
	Qnode* tail;//指向队列尾
	int size;//记录队列有效元素个数
}Queue;

//队列的初始化
extern void QueueInit(Queue* pq);
//队列为空
extern bool QueueEmpty(const Queue* pq);
//入队列
extern void QueuePush(Queue* pq, QDataType x);
//出队列
extern void QueuePop(Queue* pq);
//获取队列头部元素
extern QDataType QueueFront(const Queue* pq);
//获取队列尾部元素
extern QDataType QueueBack(const Queue* pq);
//获取队列元素个数
extern int QueueSize(const Queue* pq);
//销毁队列
extern void QueueDestroy(Queue* pq);



