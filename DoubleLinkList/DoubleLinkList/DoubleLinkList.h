#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int DataType;
typedef struct DLTNode
{
	DataType val;
	struct DLTNode* next;
	struct DLTNode* prev;
}DLTNode;
//新增节点
extern DLTNode* BuyOneNode(DataType x);
//创建哨兵节点
extern DLTNode* DLTCreat(void);
//打印链表
extern void DLTPrint(DLTNode* phead);
//判断链表是否为空
extern bool DLTEmpty(DLTNode* phead);
//头插
extern void DLTPushFront(DLTNode* phead, DataType x);
//尾插
extern void DLTPushBack(DLTNode* phead, DataType x);
//头删
extern void DLTPopFront(DLTNode* phead);
//尾删
extern void DLTPopBack(DLTNode* phead);
//查找
extern DLTNode* DLTFind(DLTNode* phead, DataType x);//返回指针方便后续插入删除等操作
//pos前插入
extern void DLTInsert(DLTNode* phead, DLTNode* pos, DataType x);
//删除pos处节点
extern void DLTErase(DLTNode* phead, DLTNode* pos);
//销毁(传一级指针不能置空实参)
extern void DLTDestory(DLTNode* phead);
