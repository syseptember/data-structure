#define _CRT_SECURE_NO_WARNINGS 1
#include "DoubleLinkList.h"

//创建哨兵位节点
DLTNode* DLTCreat()
{
	DLTNode* head = (DLTNode*)malloc(sizeof(DLTNode));
	assert(head);
	//哨兵位节点next，prev指向自己
	head->next = head;
	head->prev = head;
	head->val = -1;
	return head;
}

//创建哨兵节点
DLTNode* BuyOneNode(DataType x)
{
	DLTNode* newnode = (DLTNode*)malloc(sizeof(DLTNode));
	assert(newnode);
	newnode->val = x;
	assert(newnode);
	return newnode;
}

//打印链表
void DLTPrint(DLTNode* phead)
{
	assert(phead);
	DLTNode* cur = phead->next;//从哨兵节点后一个节点开始打印
	printf("guard<==>");
	while (cur != phead)//遍历到尾节点
	{
		printf("%d<==>", cur->val);
		cur = cur->next;
	}
	puts("");
}

//判断链表是否为空
bool DLTEmpty(DLTNode* phead)
{
	return phead == phead->next;
}
//头插
void DLTPushFront(DLTNode* phead, DataType x)
{
	assert(phead);
	
	/*DLTNode* first = phead->next;
	DLTNode* newnode = BuyOneNode(x);
	newnode->next = first;
	newnode->prev = phead;
	phead->next = newnode;
	first->prev = newnode;*/

	//复用插入
	DLTInsert(phead, phead->next, x);
}

//尾插
void DLTPushBack(DLTNode* phead, DataType x)
{
	assert(phead);
	
	/*DLTNode* tail = phead->prev;
	DLTNode* newnode = BuyOneNode(x);
	tail->next = newnode;
	newnode->prev = tail;
	newnode->next = phead;
	phead->prev = newnode;*/

	//复用插入,哨兵节点前面插入就是尾插
	DLTInsert(phead, phead, x);
}

//头删
void DLTPopFront(DLTNode* phead)
{
	assert(phead);
	assert(!DLTEmpty(phead));//链表为空不可以删除

	/*DLTNode* first = phead->next;
	DLTNode* second = first->next;
	phead->next = second;
	second->prev = phead;
	free(first);*/

	//复用删除
	DLTErase(phead, phead->next);

}

//尾删
void DLTPopBack(DLTNode* phead)
{
	assert(phead);
	assert(!DLTEmpty(phead));//链表为空不可以删除

	/*DLTNode* tail = phead->prev;
	DLTNode* tailPrev = tail->prev;
	phead->prev = tailPrev;
	tailPrev->next = phead;
	free(tail);
	*/

	//复用删除
	DLTErase(phead, phead->prev);
}

//查找
DLTNode* DLTFind(DLTNode* phead, DataType x)
{
	assert(phead);
	DLTNode* cur = phead->next;
	while (cur != phead)
	{
		if (cur->val == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

//pos前插入
void DLTInsert(DLTNode* phead, DLTNode* pos, DataType x)
{
	assert(phead);
	assert(pos);
	DLTNode* posPrev = pos->prev;
	DLTNode* newnode = BuyOneNode(x);
	newnode->prev = posPrev;
	posPrev->next = newnode;
	newnode->next = pos;
	pos->prev = newnode;
}

//删除pos处节点
void DLTErase(DLTNode* phead, DLTNode* pos)
{
	assert(phead);
	DLTNode* posNext = pos->next;
	DLTNode* posPrev = pos->prev;
	posPrev->next = posNext;
	posNext->prev = posPrev;
	free(pos);
}

//销毁(传一级指针不能置空实参)
void DLTDestory(DLTNode* phead)
{
	assert(phead);
	DLTNode* cur = phead->next;//从第一个节点开始
	while (cur != phead)
	{
		DLTNode* next = cur->next;
		free(cur);
		cur = NULL;
		cur = next;
	}
	free(phead);//销毁哨兵节点
}