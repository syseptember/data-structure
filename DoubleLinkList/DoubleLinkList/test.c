#define _CRT_SECURE_NO_WARNINGS 1
#include "DoubleLinkList.h"
//����ͷ��
void test1(DLTNode* plist)
{
	DLTPushFront(plist, 1);
	DLTPushFront(plist, 2);
	DLTPushFront(plist, 3);
	DLTPrint(plist);
}

//����β��
void test2(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPrint(plist);
}

//����ͷɾ
void test3(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPopFront(plist);
	DLTPopFront(plist);
	DLTPrint(plist);
}

//����βɾ
void test4(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPopBack(plist);
	DLTPopBack(plist);
	DLTPrint(plist);
}

//���Բ���
void test5(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPushBack(plist, 4);
	DLTNode* pos = DLTFind(plist, 4);
	//�ҵ���
	if (pos)
		pos->val *= 10;
	DLTPrint(plist);
}

//����posǰ����
void test6(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPushBack(plist, 4);
	DLTInsert(plist, DLTFind(plist, 1), 11);
	DLTInsert(plist, DLTFind(plist, 4), 11);
	DLTPrint(plist);
}

//����ɾ��pos���ڵ�
void test7(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPushBack(plist, 4);
	DLTErase(plist, DLTFind(plist, 1));
	DLTErase(plist, DLTFind(plist, 4));
	DLTPrint(plist);
}

//��������
void test8(DLTNode* plist)
{
	DLTPushBack(plist, 1);
	DLTPushBack(plist, 2);
	DLTPushBack(plist, 3);
	DLTPushBack(plist, 4);
	DLTDestory(plist);
}
int main()
{
	DLTNode* plist = DLTCreat();
	//test1(plist);
	test2(plist);
	//test3(plist);
	//test4(plist);
	//test5(plist);
	//test6(plist);
	//test7(plist);
	//test8(plist);
	return 0;
}
