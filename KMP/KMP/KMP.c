#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
//#include <assert.h>
//
//int BF(const char* S, const char* P)
//{
//	assert(S && P);
//	int i = 0;
//	int j = 0;
//	int lenS = strlen(S);
//	int lenP = strlen(P);
//	while (i < lenS && j < lenP)
//	{
//		if (S[i] == P[j])
//		{
//			i++;
//			j++;
//		}
//		else
//		{
//			i = i - j + 1;
//			j = 0;
//		}
//	}
//	if (j >= lenP)
//	{
//		return i - j;
//	}
//	return -1;
//}
//int main()
//{
//	char str[] = "aaaaaabc";
//	char find[] = "aaab";
//	printf("%d\n", BF(str, find));
//}

void GetNext(int* next, const char* P, size_t lenP)
{
	assert(next && P);
	next[0] = -1;
	next[1] = 0;
	size_t j = 2;//下一项
	size_t k = 0;//前一项的k
	while (j < lenP)//next数组为遍历完
	{
		//k有可能回退到起始位置
		if (k == -1 || P[j - 1] == P[k])
		{
			next[j] = k + 1;
			j++;
			k++;
		}
		else
		{
			k = next[k];
		}
	}
}
//优化next数组
void GetNextVal(int* nextVal, const int* next, const char* P)
{
	assert(nextVal && next);
	nextVal[0] = -1;
	for (size_t i = 1; i < strlen(P); i++)
	{
		//回退位置的字符等于当前字符，回退到那个位置的nextVal值处
		if (P[i] == P[next[i]])
		{
			nextVal[i] = nextVal[next[i]];
		}
		//回退的位置的字符不等于当前字符，回退到当前位置的next值处
		else
		{
			nextVal[i] = next[i];
		}
	}
}
int KMP(const char* S, const char* P)
{
	assert(S && P);
	int lenS = strlen(S);
	int lenP = strlen(P);
	int* next = (int*)malloc(sizeof(int) * lenP);
	int* nextVal = (int*)malloc(sizeof(int) * lenP);
	assert(next);
	GetNext(next, P, lenP);
	GetNextVal(nextVal, next, P);
	int i = 0;
	int j = 0;
	while (i < lenS && j < lenP)
	{
		if ((j == -1) || (S[i] == P[j]))
		{
			i++;
			j++;
		}
		else
		{
			j = nextVal[j];
		}
	}
	free(next);
	if (j >= lenP)
		return i - j;
	return -1;
}

int main()
{
	char* str = "ababcabcdabcde";
	char* sub = "abcd";
	printf("%d\n", KMP(str, sub));
	return 0;
}