#define _CRT_SECURE_NO_WARNINGS 1
#include "BTree.h"

BTNode* BuyOneNode(int x)
{
	BTNode* node = (BTNode*)malloc(sizeof(BTNode));
	assert(node);
	node->left = node->right = NULL;
	node->x = x;
	return node;
}
BTNode* CreatBTree()
{
	BTNode* root = BuyOneNode(1);
	root->left = BuyOneNode(2);
	root->right = BuyOneNode(3);
	root->left->left = BuyOneNode(4);
	root->right->right = BuyOneNode(5);
	root->right->left = BuyOneNode(6);
	return root;
}
void test()
{
	BTNode* root = CreatBTree();
	//前序遍历
	PreOrder(root);
	printf("\n");
	//中序遍历
	InOrder(root);
	printf("\n");
	//后序遍历
	PostOrder(root);
	printf("\n");
	//层序遍历
	LevelOrder(root);
	printf("\n");
	//printf("树的节点个数:%d\n", BTreeSize(root));
	//printf("叶子节点个数:%d\n", BTreeLeafSize(root));
	//printf("第3层节点个数:%d\n", BTreeLevelSize(root, 3));
	//printf("树的高度:%d\n", BTreeHeight(root));
	////printf("%d\n", BTreeFind(root, 7));
	//printf("%p\n", BTreeFind(root, 6));
	BTreeDestroy(root);
}
void testOj()
{
	BTNode* root = CreatBTree();
	printf("%d\n", isUnivalTree(root));
}
int main()
{
	test();
	//testOj();
	return 0;
}