#define _CRT_SECURE_NO_WARNINGS 1
#include "BTree.h"
//BTNode* BTreeFind(BTNode* root, BTNodeDataType x)
//{
//	//把容易判断的写在前面
//	//空节点没有找到
//	if (root == NULL)
//		return NULL;
// if (root->x == x)
//		return root;
//	//每一层都需要返回
//	BTNode* ret1 = BTreeFind(root->left, x);
//	if (ret1)
//		return ret1;
//	BTNode* ret2 = BTreeFind(root->right, x);
//	if (ret2)
//		return ret2;
//	//没有找到
//	return NULL;
//}

//二叉树的销毁
void BTreeDestroy(BTNode* root)
{
	if (root == NULL)
		return;
	//递归到叶子节点直接销毁
	if (root->left == NULL && root->right == NULL)
	{
		free(root);
		return;
	}
	//后序方式销毁
	BTreeDestroy(root->left);//销毁左子树
	BTreeDestroy(root->right);//销毁右子树
	free(root);//销毁当前节点
}
//前序遍历
void PreOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("N->");
		return;
	}
	//根
	printf("%d->", root->x);
	//左子树
	PreOrder(root->left);
	//右子树
	PreOrder(root->right);
}

//中序遍历
void InOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("N->");
		return;
	}
	InOrder(root->left);
	printf("%d->", root->x);
	InOrder(root->right);
}

//后续遍历
void PostOrder(BTNode* root)
{
	if (root == NULL)
	{
		printf("N->");
		return;
	}
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d->", root->x);
}

//获取节点个数
int BTreeSize(BTNode* root)
{
	return root == NULL ? 0 : 1 + BTreeSize(root->left) + BTreeSize(root->right);
}

//获取叶子节点个数
int BTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	if (root->left == NULL && root->right == NULL)
		return 1;
	return BTreeLeafSize(root->left) + BTreeLeafSize(root->right);
}

//获取第k层节点个数
int BTreeLevelSize(BTNode* root, int k)
{
	//空节点不算个数
	if (root == NULL)
		return 0;
	
	if (k == 1)
		return 1;
	return BTreeLevelSize(root->left, k - 1) + BTreeLevelSize(root->right, k - 1); 
}

//求二叉树高度
int BTreeHeight(BTNode* root)
{
	//空节点不算高度
	if (root == NULL)
		return 0;
	//叶子节点算1层
	if (root->left == NULL && root->right == NULL)
		return 1;
	//这样写可以避免时间复杂度太高
	int ret1 = BTreeHeight(root->left) + 1;
	int ret2 = BTreeHeight(root->right) + 1;
	return ret1 > ret2 ? ret1 : ret2;
}
////寻找二叉树节点(返回Bool值)
//bool BTreeFind(BTNode* root, BTNodeDataType x)
//{
//	if (root == NULL)
//		return false;
//	if (root->x == x)
//		return true;
//	return BTreeFind(root->left, x) || BTreeFind(root->left, x);
//}

//寻找二叉树节点(返回地址)
BTNode* BTreeFind(BTNode* root, int x)
{
	if (root == NULL)
		return NULL;
	if (x == root->x)
		return root;
	BTNode* ret1 = BTreeFind(root->left, x);
	if (ret1 != NULL)
		return ret1;
	//左子树中没有找到目标节点
	BTNode* ret2 = BTreeFind(root->right, x);
		return ret2;
}

//判断是否为单值二叉树
bool isUnivalTree(struct BTNode* root)
{
	//空树满足单值二叉树
	if (root == NULL)
		return true;
	//一个节点满足单值二叉树
	if (root->left == NULL && root->right == NULL)
		return true;
	//不是单只二叉树的情况
	if (root->left != NULL && root->left->x != root->x)
		return false;
	if (root->right != NULL && root->right->x != root->x)
		return false;
	//左右子树都是单值二叉树
	return isUnivalTree(root->left) && isUnivalTree(root->right);
}

//判断是否为完全二叉树
bool isCompleteBTree(BTNode* root)
{

}
