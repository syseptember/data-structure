#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int BTNodeDataType;
//二叉链
typedef struct BTNode
{
	BTNodeDataType x;
	struct BTNode* left;
	struct BTNode* right;
}BTNode;
//三叉链
typedef struct BinaryThreeNode
{
	BTNodeDataType x;
	struct BinaryThreeNode* _left;
	struct BinaryThreeNode* _right;
	struct BinaryThreeNode* _parent;
}BT3Node;
//二叉树的销毁
extern void BTreeDestroy(BTNode* root);
// 二叉树前序遍历
void PreOrder(BTNode* root);
// 二叉树中序遍历
extern void InOrder(BTNode* root);
// 二叉树后序遍历
extern void PostOrder(BTNode* root);
//二叉树的层序遍历
extern void LevelOrder(BTNode* root);
//获取二叉树节点个数
extern int BTreeSize(BTNode* root);
//获取二叉树叶子节点个数
extern int BTreeLeafSize(BTNode* root);
//获取第k层节点个数
extern int BTreeLevelSize(BTNode* root, int k);
//求二叉树高度
extern int BTreeHeight(BTNode* root);
////寻找二叉树节点(返回bool)
//extern bool BTreeFind(BTNode* root, int x);
//寻找二叉树节点(返回地址)
extern BTNode* BTreeFind(BTNode* root, BTNodeDataType x);
//判断是否为单值二叉树
extern bool isUnivalTree(BTNode* root);
//判断二叉树是否为完全二叉树
extern bool isCompleteBTree(BTNode* root);
