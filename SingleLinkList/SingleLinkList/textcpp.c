#define _CRT_SECURE_NO_WARNINGS 1
#include "SingleLinkList.h"

void text()
{								
	SLTNode* pHead = NULL;
	LinkListPushFront(&pHead, 10);
	LinkListPushFront(&pHead, 10);
	LinkListPushFront(&pHead, 1);
	LinkListPushFront(&pHead, 10);
	LinkListPushFront(&pHead, 10);
	//LinkListPushBack(&pHead, 1);
	//LinkListPushBack(&pHead, 1);
	//LinkListPopBack(&pHead);
	//LinkListPopFront(&pHead);
	//LinkListDelete(&pHead, 2);
	//LinkListInsertBefore(&pHead, 2, 7);
	PrintSLT(pHead);
	LinkListFind(&pHead, 10);
	//LinkListModify(&pHead, 7, 8);
	//LinkListDestory(&pHead);
	LinkListDestory(&pHead);
	PrintSLT(pHead);

}
int main()
{
	text();
}
