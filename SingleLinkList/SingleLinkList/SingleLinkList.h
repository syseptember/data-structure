#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
typedef int LinkListType;
typedef struct SLinkList
{
	LinkListType data;
	struct SLinkList* next;
}SLTNode;

//��ӡ
void PrintSLT(SLTNode* ppHead);
void CreatLinkList(SLTNode* ppHead);
//β��
void LinkListPushBack(SLTNode** ppHead, LinkListType x);
//ͷ��
void LinkListPushFront(SLTNode** ppHead, LinkListType x);
//βɾ
void LinkListPopBack(SLTNode** ppHead);
//ͷɾ
void LinkListPopFront(SLTNode** ppHead);
//����
void LinkListInsertBefore(SLTNode** ppHead, int pos, int key);
//ɾ��
void LinkListDelete(SLTNode** ppHead, int pos);
//����
void LinkListDestory(SLTNode** ppHead);
//����
SLTNode* LinkListSearch(SLTNode** ppHead, int key);
void LinkListFind(SLTNode** ppHead, int key);
//�޸�
void LinkListModify(SLTNode** ppHead, int src, int dst);
