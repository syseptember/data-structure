#define _CRT_SECURE_NO_WARNINGS 1
#include "SingleLinkList.h"

void PrintSLT(SLTNode* pHead)
{
	if (pHead == NULL)//链表中没有元素
	printf("No element in List");
	SLTNode* cur = pHead;
	//没有遍历到尾节点
	while (cur != NULL)
	{
		printf("%d->", cur->data);
		//指针指向下一个节点
		cur = cur->next;
	}
	printf("\n");
}

void LinkListPushBack(SLTNode** ppHead, LinkListType x)
{
	//Creat a newnode
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	newnode->data = x;
	newnode->next = NULL;
	//链表中无元素
	if (*ppHead == NULL)
	{
		*ppHead = (SLTNode*)malloc(sizeof(SLTNode));
		(*ppHead)->next = NULL;
		(*ppHead)->data = x;
	}
	//找到原来的尾节点
	else
	{
		SLTNode* tail = *ppHead;
		while (tail->next != NULL)
		{
			tail = tail->next;
		}
		tail->next = newnode;
	}
}

void LinkListPushFront(SLTNode** ppHead, LinkListType x)
{
	//Creat a newnode
	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
	newnode->data = x;
	newnode->next = *ppHead;
	*ppHead = newnode;
}

void LinkListPopBack(SLTNode** ppHead)
{
	assert(*ppHead);
	SLinkList* tail = *ppHead;
	//前驱节点
	SLinkList* pre = tail;
	//只有一个元素
	if (NULL == tail->next)
	{
		free(*ppHead);
		*ppHead = NULL;
	}
	else
	{
		while (tail->next != NULL)
		{
			pre = tail;
			tail = tail->next;
		}
		pre->next = NULL;
		free(tail);
		tail = NULL;
	}
}

void LinkListPopFront(SLTNode** ppHead)
{
	assert(*ppHead);
	SLTNode* cur = *ppHead;
	(*ppHead) = (*ppHead)->next;
	free(cur);
}

SLTNode* LinkListSearch(SLTNode** ppHead, int key)
{
	//保证链表不为空
	assert(*ppHead);
	SLTNode* cur = *ppHead;
	while (cur)
	{
		if (cur->data != key)
		{
			cur = cur->next;
		}
		else
		{
			break;
		}
	}
	return cur;
}

void LinkListFind(SLTNode** ppHead, int key)
{
	int i = 1;
	while ((*ppHead)->next)
	{
		if (LinkListSearch(ppHead, key))
		{
			printf("在链表中找到%d个%d,%p->%d\n", i++, key, (*ppHead)->next, key);
		}
		*ppHead = (*ppHead)->next;
	}
}

void LinkListInsertBefore(SLTNode** ppHead, int pos, int key)
{
	//插入合法的位置
	assert(pos >= 0);
	if (pos == 1)
	{
		LinkListPushFront(ppHead, key);
		return;
	}
	else
	{
		SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
		newnode->data = key;
		SLTNode* cur = *ppHead;
		int cnt = 1;
		//循环pos-1次将找到下标为pos的节点
		while (cnt < pos)
		{
			cur = cur->next;
			cnt++;
		}
		newnode->next = cur;
		//找到下标为pos-1的节点
		cur = *ppHead;
		while (cur->next != newnode->next)
		{
			cur = cur->next;
		}
		cur->next = newnode;
	}
}

void LinkListDelete(SLTNode** ppHead, int pos)
{
	
	if (pos == 1)
	{
		LinkListPopFront(ppHead);
		return;
	}
	//Find pos-1
	SLTNode* cur = *ppHead;
	int cnt = 0;
	//循环pos-2次找到第pos-1个节点
	while (cnt < pos - 2)
	{
		cur = cur->next;
		cnt++;
	}
	SLTNode* tmp = cur->next;
	//将pos-1的next成员赋值为下一个节点的next成员
	cur->next = cur->next->next;
	free(tmp);
	
}

void LinkListModify(SLTNode** ppHead, int src, int dst)
{
	assert(*ppHead);
	SLTNode* cur = LinkListSearch(ppHead, src);
	if (cur)
	{
		cur->data = dst;
	}
	else printf("链表中没有待修改元素%d\n", src);
}

void LinkListDestory(SLTNode** ppHead)
{
	free(*ppHead);
	*ppHead = NULL;
}

