#define _CRT_SECURE_NO_WARNINGS 1
#include "SingleLinkList.h"

//新建节点
SLTNode* CreatListNode(int x)
{
	SLTNode* newNode = (SLTNode*)malloc(sizeof(SLTNode));
	assert(newNode);
	newNode->val = x;
	newNode->next = NULL;
	return newNode;
}
//头插
void SingleListPushFront(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	SLTNode* newNode = CreatListNode(x);
	newNode->next = *pphead;
	*pphead = newNode;
}
//头删
void SingleListPopFront(SLTNode** pphead)
{
	//防止链表为空
	assert(pphead && *pphead);
	SLTNode* cur = *pphead;
	*pphead = cur->next;
	free(cur);
}

//尾插
void SingleListPushBack(SLTNode** pphead, SLTDataType x)
{
	assert(pphead);
	//链表为空
	if (*pphead == NULL)
	{
		*pphead = CreatListNode(x);
		return;
	}
	SLTNode* cur = *pphead;
	//找尾
	while (cur->next)
	{
		cur = cur->next;
	}
	//插入
	SLTNode* newTail = CreatListNode(x);
	cur->next = newTail;
}

//尾删
void SingleListPopBack(SLTNode** pphead)
{
	//判断链表是否为空
	assert(pphead && *pphead);
	//链表只有一个节点
	SLTNode* cur = *pphead;
	if (cur->next == NULL)
	{
		*pphead = NULL;
		return;
	}
	//链表有多个节点
	SLTNode* prev = NULL;
	//找尾
	while (cur->next)
	{
		prev = cur;
		cur = cur->next;
	}
	//删除
	prev->next = NULL;
	free(cur);
}
//查找（返回节点得地址）-->返回地址方便后续直接通过该地址插入/删除数据
SLTNode* SingleListFind(SLTNode* phead, SLTDataType x)
{
	SLTNode* cur = phead;
	while (cur)
	{
		if (x == cur->val)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

//修改
void SingleListModify(SLTNode* phead,SLTDataType src, SLTDataType dest)
{
	SLTNode* pos = SingleListFind(phead, src);
	if (pos)
		pos->val = dest;
}

//插入->在pos位置后面插入数据	
void SingleListInsertAfter(SLTNode* pos, SLTDataType x)
{
	assert(pos);
	SLTNode* newNode = CreatListNode(x);
	newNode->next = pos->next;
	pos->next = newNode;
}
//删除
void SingleListEraseAfter(SLTNode* pos)
{
	//不能删除最后一个节点得后一个
	assert(pos && pos->next);
	SLTNode* tmp = pos->next;
	pos->next = pos->next->next;
	free(tmp);
}
//打印
void SingleListPrint(SLTNode* phead)
{
	SLTNode* cur = phead;
	while (cur)
	{
		printf("%d->", cur->val);
		cur = cur->next;
	}
	printf("NULL\n");
}

//销毁
void SingleListDestory(SLTNode** pphead)
{                       
	//不能销毁空链表
	assert(pphead && *pphead);
	SLTNode* cur = *pphead;
	while (cur)
	{
		SLTNode* tmp = cur->next;
		free(cur);
		cur = tmp;
	}
	*pphead = NULL;
}
