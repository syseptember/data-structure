#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
typedef int SLTDataType;
typedef struct SLTNode
{
	SLTDataType val;
	struct SLTNode* next;
}SLTNode;

//新建节点
extern SLTNode* CreatListNode(SLTDataType x);
//头插
extern void SingleListPushFront(SLTNode** pphead, SLTDataType x);
//头删
extern void SingleListPopFront(SLTNode** pphead);
//尾插
extern void SingleListPushBack(SLTNode** pphead, SLTDataType x);
//尾删
extern void SingleListPopBack(SLTNode** pphead);
//查找
extern SLTNode* SingleListFind(SLTNode* phead, SLTDataType x);
//修改
extern void SingleListModify(SLTNode* phead, SLTDataType src, SLTDataType dest);
//pos位置后插入一个节点(参数不需要头节点指针)
extern void SingleListInsertAfter(SLTNode* pos, SLTDataType
	x);
//删除pos位置后一个节点(参数不需要头节点指针)
extern void SingleListEraseAfter(SLTNode* pos);
//打印
extern void SingleListPrint(SLTNode* phead);
//销毁
extern void SingleListDestory(SLTNode** phead);

