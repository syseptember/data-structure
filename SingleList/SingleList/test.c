#define _CRT_SECURE_NO_WARNINGS 1
#include "SingleLinkList.h"

//����ͷ��β��
void test1()
{
	SLTNode* plist = NULL;
	SingleListPushFront(&plist, 1);
	SingleListPushFront(&plist, 2);
	SingleListPushFront(&plist, 3);
	SingleListPopFront(&plist);
	SingleListPopFront(&plist);
	SingleListPopFront(&plist);
	SingleListPrint(plist);
}
//����ͷɾ
void test2()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushBack(&plist, 3);
	SingleListPopFront(&plist);
	SingleListPopFront(&plist);
	SingleListPopFront(&plist);
	SingleListPrint(plist);
}
//����βɾ
void test3()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushFront(&plist, 3);
	SingleListPushFront(&plist, 4);
	SingleListPopBack(&plist);
	SingleListPopBack(&plist);
	SingleListPopBack(&plist);
	SingleListPopBack(&plist);
	SingleListPrint(plist);
}
//���Բ���
void test4()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushBack(&plist, 3);
	SingleListPushBack(&plist, 4);
	SLTNode* pos = SingleListFind(plist, 4);
	if (pos)
		printf("%d", pos->val);
}

//���Բ���
void test5()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushBack(&plist, 3);
	SingleListPushBack(&plist, 4);
	SLTNode* pos = NULL;
	pos = SingleListFind(plist, 2);
	if (pos)
		SingleListInsertAfter(pos, 6);
	pos = SingleListFind(plist, 4);
	if (pos)
		SingleListInsertAfter(pos, 7);
	pos = SingleListFind(plist, 1);
	if (pos)
		SingleListInsertAfter(pos, 0);

	SingleListPrint(plist);
}
//����ɾ��
void test6()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushBack(&plist, 3);
	SingleListPushBack(&plist, 4);
	SingleListPushBack(&plist, 5);
	SLTNode* pos = NULL;
	pos = SingleListFind(plist, 1);
	if (pos)
		SingleListEraseAfter(pos);
	pos = SingleListFind(plist, 4);
	if (pos)
		SingleListEraseAfter(pos);
	SingleListPrint(plist);
	//����ɾ�����һ���ڵ�ú���һ��
	pos = SingleListFind(plist, 4);
	if (pos)
		SingleListEraseAfter(pos);
}
//��������
void test7()
{
	SLTNode* plist = NULL;
	SingleListPushBack(&plist, 1);
	SingleListPushBack(&plist, 2);
	SingleListPushBack(&plist, 3);
	SingleListPrint(plist);
	SingleListDestory(&plist);
	SingleListPrint(plist);
}
int main()
{
	//test1();
	//test2();
	//test3();
	//test4();
	//test5();
	//test6();
	test7();
	return 0;
}
