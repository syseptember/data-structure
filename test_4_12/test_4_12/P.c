#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
//// 请计算一下Func1中++count语句总共执行了多少次？
//void Func1(int N)
//{
//	int count = 0;
//	for (int i = 0; i < N; ++i)
//	{
//		for (int j = 0; j < N; ++j)
//		{
//			++count;
//		}
//	}
//
//	for (int k = 0; k < 2 * N; ++k)
//	{
//		++count;
//	}
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//}
//int main()
//{
//	Func1(100);
//	return 0;
//}

//// 计算Func2的时间复杂度？
//void Func2(int N)
//{
//	int count = 0;
//	for (int k = 0; k < 2 * N; ++k)
//	{
//		count++;
//	}
//	int M = 10;
//	while (M--)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//}
// 计算Func3的时间复杂度？
//void Func3(int N, int M)
//{
//
//	int count = 0;
//	++count;
//	int count = 0;
//	for (int k = 0; k < M; ++k)
//	{
//		++count;
//	}
//	for (int k = 0; k < N; ++k)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//}

//// 计算Func4的时间复杂度？
//void Func4(int N)
//{
//	int count = 0;
//	for (int k = 0; k < 100; ++k)
//	{
//		++count;
//	}
//	printf("%d\n", count);
//}

//// 计算strchr的时间复杂度？
//const char* strchr(const char* str, int character);
//
//// 计算阶乘递归Fac的时间复杂度？
//long long Fac(size_t N)
//{
//	if (0 == N)
//		return 1;
//
//	return Fac(N - 1) * N;
//}

//// 计算斐波那契递归Fib的时间复杂度？
//long long Fib(size_t N)
//{
//	if (N < 3)
//		return 1;
//
//	return Fib(N - 1) + Fib(N - 2);
//}

//空间复杂度
//// 计算BubbleSort的空间复杂度？
////只会开辟一个变量的空间
//void BubbleSort(int* a, int n)
//{
//	assert(a);
//	for (size_t end = n; end > 0; --end)
//{
//	int exchange = 0;
//	for (size_t i = 1; i < end; ++i)
//	{
//		if (a[i - 1] > a[i])
//		{
//			Swap(&a[i - 1], &a[i]);
//			exchange = 1;
//		}
//	}
//	if (exchange == 0)
//		break;
// }
//}

// 计算Fibonacci的空间复杂度？
//// 返回斐波那契数列的前n项
//long long* Fibonacci(size_t n)
//{
//	if (n == 0)
//		return NULL;
//
//	long long* fibArray = (long long*)malloc((n + 1) * sizeof(long long));
//	fibArray[0] = 0;
//	fibArray[1] = 1;
//	for (int i = 2; i <= n; ++i)
//	{
//		fibArray[i] = fibArray[i - 1] + fibArray[i - 2];
//	}
//	return fibArray;
//}

//计算fib递归函数的空间复杂度
int fib(int n)
{
	if (n < 3)
		return 1;
	else
		return fib(n - 1) + fib(n - 2);
}

//int main()
//{
//	long long result = fib(4);
//	return 0;
//}

//void fun1()
//{
//	int a = 10;
//	printf("%p\n", &a); 
//}
//void fun2()
//{
//	printf("hehe\n");
//	int b = 0;
//	printf("hehe\n");
//	printf("%p\n", &b);
//}
////void fun3()
////{
////	fun1();
////	fun2();
////}
//int main()
//{
//	fun1();
//	fun2();
//	return 0;
//}