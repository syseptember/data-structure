#define _CRT_SECURE_NO_WARNINGS 1
#include "sort.h"
#include <time.h>
#include <stdlib.h>
#define N 1000000
//测试排序性能
void TestOP()
{
	srand(time(NULL));
	int* a1 = (int*)malloc(sizeof(int) * N);
	int* a2 = (int*)malloc(sizeof(int) * N);
	int* a3 = (int*)malloc(sizeof(int) * N);
	int* a4 = (int*)malloc(sizeof(int) * N);
	int* a5 = (int*)malloc(sizeof(int) * N);
	int* a6 = (int*)malloc(sizeof(int) * N);
	int* a7 = (int*)malloc(sizeof(int) * N);
	int* a8 = (int*)malloc(sizeof(int) * N);
	assert(a1 && a2 && a3 && a4 && a5 && a6 && a7 && a8);
	//随机生成100000个数
	for (int i = 0; i < N; i++)
	{
		a1[i] = rand();
		a2[i] = a1[i];
		a3[i] = a1[i];
		a4[i] = a1[i];
		a5[i] = a1[i];
		a6[i] = a1[i];
		a7[i] = a1[i];
		a8[i] = a1[i];
	}

	size_t start1 = clock();
	//InsertSort(a1, N);
	size_t end1 = clock();

	size_t start2 = clock();
	ShellSort(a2, N);
	size_t end2 = clock();

	size_t start3 = clock();
	//BubbleSort(a3, N);
	size_t end3 = clock();

	size_t start4 = clock();
	//SelectSort(a4, N);
	size_t end4 = clock();

	size_t start5 = clock();
	HeapSort(a5, N);
	size_t end5 = clock();

	size_t start6 = clock();
	QuickSort(a5, 0, N - 1);
	size_t end6 = clock();

	size_t start7 = clock();
	MergeSort(a5, N);
	size_t end7 = clock();

	size_t start8 = clock();
	CountSort(a5, N);
	size_t end8 = clock();

	printf("InsertSort_Time:%ums\n", end1 - start1);
	printf("ShellSort_Time:%ums\n", end2 - start2);
	printf("BubbleSort_Time:%ums\n", end3 - start3);
	printf("SelectSort_Time:%ums\n", end4 - start4);
	printf("HeapSort_Time:%ums\n", end5 - start5);
	printf("QuickSort_Time:%ums\n", end6 - start6);
	printf("MergeSort_Time:%ums\n", end7 - start7);
	printf("CountSort_Time:%ums\n", end8 - start8);

	free(a1);
	free(a2);
	free(a3);
	free(a4);
	free(a5);
	free(a6);
	free(a7);
	free(a8);
}

//冒泡排序
void TestBubbleSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	BubbleSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);

}
//直接插入排序
void TestInsetSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	InsertSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//希尔排序(数据量偏大时效率快)
void TestShellSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	ShellSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//选择排序(效率最差)
void TestSelectSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	SelectSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//堆排序(时间复杂度O(nlogn))
void TestHeapSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	HeapSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//快排(时间复杂度O(nlogn))
void TestQuickSort()
{
	int arr[] = { 6, 1, 2, 7, 9, 3, 4, 5, 10, 8 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	//QuickSort(arr, 0, sizeof(arr) / sizeof(arr[0]) - 1);
	QuickSortNonR(arr, 0, sizeof(arr) / sizeof(arr[0]) - 1);
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//归并排序
//时间复杂度O(nlogn)
//空间复杂度O(n+logn)
void TestMergeSort()
{
	int arr[] = { 3, 5, 7, 6 ,9, 2, 10, 8, 4, 100 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	MergeSort(arr, sizeof(arr) / sizeof(arr[0]));
	//MergeSortNonR(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}

//计数排序(时间复杂度O(n+gap),数据量相差不大时效率最高)
void TestCountSort()
{
	int arr[] = { 8, 2, 2, 4, 9, 10, 3, 3 };
	PrintArr(arr, sizeof arr / sizeof arr[0]);
	CountSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintArr(arr, sizeof arr / sizeof arr[0]);
}
int main()
{

	//TestInsetSort();
	//TestBubbleSort();
	//TestShellSort();
	//TestSelectSort();
	//TestHeapSort();
	TestQuickSort();
	//TestMergeSort();
	//TestCountSort();
	//TestOP();
}

