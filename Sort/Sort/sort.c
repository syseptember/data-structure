#define _CRT_SECURE_NO_WARNINGS 1
#include "sort.h"

void Swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

//打印数组
void PrintArr(int* arr, int sz)
{
	for (int i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	puts("");
}

//冒泡排序
void BubbleSort(int* arr, int sz)
{
	int exchange = 0;
	for (int i = 0; i < sz - 1; i++)
	{
		exchange = 0;
		for (int j = 0; j < sz - 1 - i; j++)
		{
			if (arr[j] > arr[j + 1])
			{
				Swap(&arr[j], &arr[j + 1]);
				exchange = 1;
			}
		}
		if (exchange == 0)
			return;
	}
}
//插入排序
void InsertSort(int* arr, int sz)
{
	//一共sz-1躺
	for (int i = 0; i < sz - 1; i++)
	{
		//一趟排序
		int end = i;
		int x = arr[end + 1];
		while (end >= 0)
		{
			if (x < arr[end])
			{
				//大于x的元素向后挪动
				arr[end + 1] = arr[end];
				end--;
			}
			else
			{
				break;
			}
		}
		//插入x
		arr[end + 1] = x;
	}
}

//希尔排序
void ShellSort(int* arr, int sz)
{
	//当gap为1时执行的是插入排序，保证数组一定是有序的
	int gap = sz;
	while (gap > 1)				//不需要gap==1,否则会多判断一圈或者死循环
	{
		//gap /= 2;				//一定可以保证gap最后一次取得1
		gap = gap / 3 + 1;	//+1保证gap最后一次取得1
		//分gap组进行预排序		
		////循环gap次，将每组都进行插入排序
		//for (int j = 0; j < gap; j++)
		//{
		//	//一组的插入排序
		//	for (int i = j; i < sz - gap; i += gap)
		//	{
		//		int end = i;
		//		int x = arr[end + gap];
		//		//一组的一趟插入排序
		//		while (end >= 0)
		//		{
		//			if (x < arr[end])
		//			{
		//				arr[end + gap] = arr[end];
		//				end -= gap;
		//			}
		//			else break;
		//		}
		//		arr[end + gap] = x;
		//	}
			//一锅炖
		for (int i = 0; i < sz - gap; i++)
		{
			int end = i;
			int x = arr[end + gap];
			while (end >= 0)
			{
				if (arr[end] > x)
				{
					arr[end + gap] = arr[end];
					end -= gap;
				}
				else break;
			}
			arr[end + gap] = x;
		}

		/*printf("增量为%d排序后的结果:", gap);
		PrintArr(arr, sz);*/
	}


}

//选择排序
void SelectSort(int* arr, int sz)
{
	int begin = 0;
	int end = sz - 1;
	while (begin < end)
	{
		int mini = begin;
		int maxi = begin;
		for (int i = begin; i <= end; i++)
		{
			if (arr[i] > arr[maxi])
				maxi = i;
			if (arr[i] < arr[mini])
				mini = i;
		}
		Swap(&arr[begin], &arr[mini]);
		//如果最大元素在最开始的位置，需要修正最大元素的下标
		if (maxi == begin)
			maxi = mini;
		Swap(&arr[end], &arr[maxi]);

		begin++;
		end--;
	}
}
//向下调整算法
void AdjustDwon(int* arr, int sz, int parent)
{
	int child = parent * 2 + 1;
	while (child < sz)
	{
		//child指向大孩子并且保证有孩子存在
		if (child + 1 < sz && arr[child] < arr[child + 1])
			child++;
		if (arr[parent] < arr[child])
			Swap(&arr[parent], &arr[child]);
		else
			break;
		parent = child;
		child = parent * 2 + 1;
	}
}
//堆排序--排升序
void HeapSort(int* arr, int sz)
{
	////向上调整建大堆
	//for (int i = 1; i < sz; i++)
	//{
	//	AdjustUp(arr, i);
	//}
	//向下调整建大堆
	for (int i = (sz - 1 - 1) / 2; i >= 0; i--)
	{
		AdjustDwon(arr, sz, i);
	}
	//排序
	for (int i = 0; i < sz - 1; i++)
	{
		Swap(&arr[0], &arr[sz - 1 - i]);
		AdjustDwon(arr, sz - 1 - i, 0);
	}
}

//hoare法
int PartSort1(int* arr, int begin, int end)
{
	//保证第一个元素不是最值
	int midi = GetMidi(arr, begin, end);
	Swap(&arr[begin], &arr[midi]);

	//以begin为keyi(keyi是基准值的下标)
	int keyi = begin;
	int L = begin;
	int R = end;
	while (L < R)//L和R相遇前
	{
		//R寻找小于key的
		while (R > L && arr[R] >= arr[keyi]) R--;
		//L寻找大于key的
		while (R > L && arr[L] <= arr[keyi]) L++;
		//交换L和R位置的值
		Swap(&arr[R], &arr[L]);
	}
	//交换key与相遇位置的值	
	Swap(&arr[keyi], &arr[L]);
	return L;
}

//挖坑法
int PartSort2(int* arr, int left, int right)
{
	//保证第一个元素不是最值
	int midi = GetMidi(arr, left, right);
	Swap(&arr[left], &arr[midi]);

	int key = arr[left];//保留首个数据到key中
	int hole = left;//第一个坑为第一个数据
	int L = left;
	int R = right;
	while (L < R)//循环条件为LR为相遇
	{
		//R找小与key的
		while (R > L && arr[R] >= key) R--;
		arr[hole] = arr[R];//将R位置的值填入坑中
		hole = R;//坑变为R所处的位置

		//L找大于key的
		while (R > L && arr[L] <= key) L++;
		arr[hole] = arr[L];//将L位置的值填入坑中
		hole = L;//坑变为L所处的位置
	}
	arr[R] = key;//将key填入相遇处的坑
	return R;//返回相遇位置
}

//前后指针法
int PartSort3(int* arr, int left, int right)
{
	//保证第一个元素不是最值
	int midi = GetMidi(arr, left, right);
	Swap(&arr[left], &arr[midi]);

	int prev = left;						//prev指向第一个元素
	int cur = prev + 1;						//cur指向prev后面一个元素
	int keyi = left;						//key的值选为待排数据第一个元素
	while (cur <= right)					//循环条件为cur为超过右边界
	{
		if (arr[cur] < arr[keyi] && ++prev != cur)			//cur指向的值小于prev,prev++并且交换与cur所指向的值
		{
			Swap(&arr[prev], &arr[cur]);//交换++prev和cur所指向的值
		}
		cur++;								//cur向后移动一位
	}
	Swap(&arr[keyi], &arr[prev]);			//交换keyi和prev所指向的值
	return prev;
}
//三数取中
int GetMidi(int* arr, int begin, int end)
{
	int midi = (begin + end) / 2;
	if (arr[begin] > arr[end])
	{
		if (arr[midi] > arr[begin])
			return begin;
		else if (arr[end] > arr[midi])
			return end;
		else
			return midi;
	}
	else //arr[begin] < arr[end]
	{
		if (arr[midi] < arr[begin])
			return begin;
		else if (arr[end] < arr[midi])
			return end;
		else
			return midi;
	}
}
//快排递归
void QuickSort(int* arr, int begin, int end)
{
	//结束条件1.区间只有一个元素 2.区间不存在  
	if (end <= begin)
		return;

	int keyi = PartSort1(arr, begin, end);
	//int keyi = PartSort2(arr, begin, end);
	//int keyi = PartSort3(arr, begin, end);
	QuickSort(arr, begin, keyi - 1);
	QuickSort(arr, keyi + 1, end);
}

//快排非递归
void QuickSortNonR(int* arr, int begin, int end)
{
	Stack st;
	StackInit(&st);
	StackPush(&st, end);
	StackPush(&st, begin);
	while (!StackEmpty(&st))
	{
		int left = StackTop(&st);
		StackPop(&st);
		int right = StackTop(&st);
		StackPop(&st);
		int keyi = PartSort1(arr, left, right);
		//如果右区间个数大于1则入栈
		if (keyi + 1 < right)
		{
			StackPush(&st, right);
			StackPush(&st, keyi + 1);
		}
		//如果左区间个数大于1则入栈
		if (keyi - 1 > left)
		{
			StackPush(&st, keyi - 1);
			StackPush(&st, left);
		}
	}
	StackDestroy(&st);
}
void _MergeSort(int* arr, int* tmp, int begin, int end)
{
	//递归终止条件--区间只有一个数
	if (begin == end)
		return;
	//小区间优化
	if (end - begin + 1 <= 10)
	{
		InsertSort(arr + begin, end - begin + 1);
		return;
	}
	//将数据均分未2组
	int mid = (begin + end) >> 1;
	int i = begin;
	//使两组均有序
	_MergeSort(arr, tmp, begin, mid);
	_MergeSort(arr, tmp, mid + 1, end);
	//有序数组归并---归并到tmp数组中
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	while (begin1 <= end1 && begin2 <= end2)
	{
		if (arr[begin1] < arr[begin2])
			tmp[i++] = arr[begin1++];
		else
			tmp[i++] = arr[begin2++];
	}
	while (begin1 <= end1)	tmp[i++] = arr[begin1++];
	while (begin2 <= end2)	tmp[i++] = arr[begin2++];
	//拷贝回元素组中
	memcpy(arr + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}
//归并排序
void MergeSort(int* arr, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) * sz);//tmp作为临时数组存放有序数组合并后的数据
	_MergeSort(arr, tmp, 0, sz - 1);		  //因为要借助临时数组所以构造子函数
	free(tmp);
}

//归并排序非递归(归并一段拷贝一段)
//void MergeSortNonR(int* arr, int sz)
//{
//	int* tmp = (int*)malloc(sizeof(int) * sz);//临时数组用来存放分组排序的结果
//	assert(tmp);
//	int gap = 1;//从间隔为1开始归并
//	while (gap < sz)
//	{
//		int j = 0;
//		for (int i = 0; i < sz; i += 2 * gap)
//		{
//			//有序数组归并---归并到tmp数组中
//			int begin1 = i, end1 = begin1 + gap - 1;
//			int begin2 = end1 + 1, end2 = begin2 + gap - 1;
//			//end1越界或者begin2越界跳出剩下为归并的数据交给下一次处理
//			if (end1 >= sz || begin2 >= sz)
//			{
//				break;
//			}
//			//end2越界,修正end2
//			if (begin2 < sz && end2 >= sz)
//			{
//				end2 = sz - 1;
//			}
//			while (begin1 <= end1 && begin2 <= end2)
//			{
//				if (arr[begin1] < arr[begin2])
//					tmp[j++] = arr[begin1++];
//				else
//					tmp[j++] = arr[begin2++];
//			}
//			while (begin1 <= end1)	tmp[j++] = arr[begin1++];
//			while (begin2 <= end2)	tmp[j++] = arr[begin2++];
//			//归并一段拷贝一段(注意begin1变化了，所以需要用i)
//			memcpy(arr + i, tmp + i, sizeof(int) * (end2 - i + 1));
//		}
//		//拷贝回元素组中
//		//memcpy(arr, tmp, sizeof(int) * sz);
//		gap *= 2;
//	}  
//	
//	free(tmp);
//}

//归并排序非递归(修正区间)
void MergeSortNonR(int* arr, int sz)
{
	int* tmp = (int*)malloc(sizeof(int) * sz);//临时数组用来存放分组排序的结果
	assert(tmp);
	int gap = 1;//从间隔为1开始归并
	while (gap < sz)
	{
		int j = 0;
		for (int i = 0; i < sz; i += 2 * gap)
		{
			//有序数组归并---归并到tmp数组中
			int begin1 = i, end1 = begin1 + gap - 1;
			int begin2 = end1 + 1, end2 = begin2 + gap - 1;
			//end1越界
			if (end1 >= sz)
			{
				//end1修正为边界
				end1 = sz - 1;
				//[begin2, end2]修正为不存在的区间
				begin2 = sz;
				end2 = sz - 1;
			}
			else if (begin2 >= sz)
			{
				//修正为不存在的区间
				begin2 = sz;
				end2 = sz - 1;
			}
			else if (end2 >= sz)
			{
				end2 = sz - 1;
			}
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (arr[begin1] < arr[begin2])
					tmp[j++] = arr[begin1++];
				else
					tmp[j++] = arr[begin2++];
			}
			while (begin1 <= end1)	tmp[j++] = arr[begin1++];
			while (begin2 <= end2)	tmp[j++] = arr[begin2++];

		}
		//拷贝回元素组中
		memcpy(arr, tmp, sizeof(int) * sz);
		gap *= 2;
	}

	free(tmp);
}

//计数排序
void CountSort(int* arr, int sz)
{
	//创建count数组
	int max = arr[0], min = arr[0];
	for (int i = 0; i < sz; i++)
	{
		arr[i] > max ? max = arr[i] : 1;
		arr[i] < min ? min = arr[i] : 1;
	}
	int* countArr = (int*)calloc(max - min + 1, sizeof(int));
	assert(countArr);
	//映射---统计原数组中数据出现的次数
	for (int i = 0; i < sz; i++)
		countArr[arr[i] - min]++;
	//排序
	for (int i = 0, j = 0; i < max - min + 1; i++)
	{
		while (countArr[i]--)
		{
			arr[j++] = i + min;
		}
	}
}

////基数排序
//#define REDIX 10
//queue <int> Q[REDIX];
//
//int GetKey(int a, int i)
//{
//	while (i--)
//	{
//		int key = a % 10;
//		a /= 10;
//	}
//	return key;
//}
//void Distrubute(int* arr, int sz, int i)
//{
//	for (int j = 0; j < sz; j++)
//	{
//		int key = GetKey(arr[j], i);
//		Q[key].push(arr[j]);
//	}
//}
//void Collect(int* arr)
//{
//	int j = 0;
//	for (int i = 0; i < REDIX; i++)
//	{
//		while (!Q[i].empty())
//		{
//			arr[j++] = Q[i].top();
//			Q[i].pop();
//
//		}
//	}
//}
//void RedixSort(int* arr, int sz)
//{
//	int bitsMax = 0;
//	for (int i = 0; i < sz; i++)
//	{
//		int bits = 0;
//		int tmp = arr[i];
//		while (tmp)
//		{
//			bits++;
//			tmp /= 10;
//			bitsMax = bitsMax > bits ? bitsMax : bits;
//		}
//	}
//	for (int i = 0; i < bitsMax; i++)
//	{
//		//分发数据
//		Distrubute(arr, sz, i + 1);
//		//回收数据
//		Collect(arr);
//	}
//}
