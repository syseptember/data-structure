#define _CRT_SECURE_NO_WARNINGS 1
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>
typedef int STDataType;
typedef struct
{
	STDataType* a;//a指向一个连续的数组
	int top;//top记录栈顶后一个元素的下标
	int capacity;
}Stack;
//栈的初始化
extern void StackInit(Stack* ps);
//销毁栈
extern void StackDestroy(Stack* ps);
//空栈
extern bool StackEmpty(const Stack* ps);
//入栈
extern void StackPush(Stack* ps, STDataType x);
//出栈
extern void StackPop(Stack* ps);
//获取栈顶元素
extern STDataType StackTop(const Stack* ps);
//获取栈元素个数
extern int StackSize(const Stack* ps);
